from flask import Flask
from controllers.login import login
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
import os


login_manager = LoginManager()
app = Flask(__name__, instance_relative_config=True, template_folder='../templates', static_folder='../static')

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/pastebox'
db = SQLAlchemy(app)
from flask_migrate import Migrate
migrate = Migrate(app, db)



def create_app():

    app.register_blueprint(login)
    login_manager.init_app(app)
    from app.models import User
    app.secret_key = os.urandom(12)
    # db.create_all()
    return app