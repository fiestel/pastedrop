from flask import Blueprint, request, redirect, url_for, render_template, session, flash

login = Blueprint('login', __name__)


@login.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return "Hello Boss! You are logged in."


@login.route('/signup', methods=['GET', "POST"])
def signup():
    data = request.data

    if request.environ['REQUEST_METHOD'] == "POST":
        return request.headers
    else:
        return "test"


@login.route('/login', methods=['POST'])
def test():
    if request.form['password'] == 'password' and request.form['username'] == 'admin':
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return home()
